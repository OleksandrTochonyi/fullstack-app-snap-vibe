import express from "express";
import passport from "passport";

import {
  addComment,
  createPost,
  deletePost,
  getAllPosts,  
  getPostById,
  updatePost,
  deleteComment
} from "./../controllers/post.js";

export const postRouter = express.Router();
const isAuthenticateMiddleware = passport.authenticate("jwt", {
  session: false,
});

postRouter
  .route("/posts")
  .get(isAuthenticateMiddleware, getAllPosts)
  .post(isAuthenticateMiddleware, createPost);

postRouter
  .route("/posts/:id")
  .get(isAuthenticateMiddleware, getAllPosts)
  .delete(isAuthenticateMiddleware, deletePost)
  .patch(isAuthenticateMiddleware, updatePost);

postRouter
.route("/post/:id")
.get(isAuthenticateMiddleware, getPostById);

postRouter
.route("/posts/comment")
.post(isAuthenticateMiddleware, addComment)

postRouter
.route("/posts/comment/:id")
.delete(isAuthenticateMiddleware, deleteComment)