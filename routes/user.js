import express from "express";
import passport from "passport";
import {
  addToFriend,
  deleteFriend,
  getAllUsers,
  getCurrentUser,
  getUserById,
  updateCurrentUser,
} from "../controllers/user.js";
import { upload } from "./../middleware/upload.js";

export const userRouter = express.Router();

const isAuthenticateMiddleware = passport.authenticate("jwt", {
  session: false,
});

userRouter
  .route("/users")
  .get(isAuthenticateMiddleware, getAllUsers)
  .patch(isAuthenticateMiddleware, upload.single('image'), updateCurrentUser);

userRouter
  .route("/user")
  .get(isAuthenticateMiddleware, getCurrentUser)
  .patch(isAuthenticateMiddleware, addToFriend);

userRouter
  .route("/users/:id")
  .get(isAuthenticateMiddleware, getUserById)
  .delete(isAuthenticateMiddleware, deleteFriend);
