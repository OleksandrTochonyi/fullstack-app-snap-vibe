import express from "express";
import { login, register } from "../controllers/auth.js";
import { upload } from "./../middleware/upload.js";
export const authRouter = express.Router();

//  /api/auth/login
authRouter.post("/login", login);

//  /api/auth/register
authRouter.post("/register", upload.single("image"), register);
