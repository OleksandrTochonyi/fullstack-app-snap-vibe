import express from "express";
import passport from "passport";
import { createMessage, getAllMessages, updateMessage, getMessagesByUserId, maskMessagesAsRead } from "./../controllers/message.js";

export const messageRouter = express.Router();

const isAuthenticateMiddleware = passport.authenticate("jwt", {
  session: false,
});

messageRouter
  .route("/messages")
  .get(isAuthenticateMiddleware, getAllMessages)
  .post(isAuthenticateMiddleware, createMessage)
  .patch(isAuthenticateMiddleware, maskMessagesAsRead)

  messageRouter
  .route("/messages/:id") 
  .patch(isAuthenticateMiddleware, updateMessage)
  .get(isAuthenticateMiddleware, getMessagesByUserId)
