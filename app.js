import express from "express";
import cors from "cors";
import morgan from "morgan";
import mongoose from "mongoose";
import passport from "passport";
import { passportJwt } from "./middleware/passport.js";

import { mongoURL } from "./config/keys.js";
import { authRouter } from "./routes/auth.js";
import { postRouter } from "./routes/post.js";
import { userRouter } from "./routes/user.js";
import { messageRouter } from './routes/message.js';

export const app = express();

mongoose
  .connect(mongoURL)
  .then(() => {
    console.log("Database connected");
  })
  .catch((err) => console.log(err));

app.use(passport.initialize());
passportJwt(passport);

app.use(morgan("dev"));
app.use(cors());
app.use(express.urlencoded({ extended: true }));
app.use(express.json());

app.use("/uploads", express.static("uploads"))
app.use("/api/auth", authRouter);
app.use("/api", postRouter);
app.use("/api", userRouter)
app.use("/api", messageRouter)