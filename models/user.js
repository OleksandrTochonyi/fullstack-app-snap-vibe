import mongoose from "mongoose";

const schema = new mongoose.Schema({
  firstName: {
    type: String,
  },
  lastName: {
    type: String,
  },
  dateOfBirthday: {
    type: String,
  },
  avatar: {
    type: String,
  },
  about: {
    type: Object,
  },
  email: {
    type: String,
    required: true,
  },
  password: {
    type: String,
    required: true,
  },
  friends: {
    type: Array,
  },
  isShowProfile: {
    type: Boolean,
  },
});

export const UserModel = mongoose.model("User", schema);
