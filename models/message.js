import mongoose from "mongoose";

const schema = new mongoose.Schema({
  sender: {
    type: String,
    require: true,
  },
  recipient: {
    type: String,
    require: true,
  },
  content: {
    type: String,
    require: true,
  },
  timestamp: {
    type: Date,
    require: true,
  },
  status: {
    type: String,
    require: true,
  },
});

export const MessageModel = mongoose.model("message", schema);
