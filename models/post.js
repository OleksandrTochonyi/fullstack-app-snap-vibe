import mongoose from "mongoose";

const schema = new mongoose.Schema({
author: {
  type: String,
  require: true
},
content: {
  type: String,
  require: true
},
timestamp: {
  type: Date,
  require: true
},
comments: {
  type: Array,
  require: true
},
likes: {
  type: Array,
  require: true
},
});

export const PostModel = mongoose.model("post", schema);
