import { PostModel } from "../models/post.js";
import { errorHandler } from "../utils/errorHandler.js";
import { UserModel } from "./../models/user.js";
import { v4 as uuidv4 } from "uuid";

export const getAllPosts = async (req, res) => {
  const getAll = req.query.getAll;
  let id = req.user.id;
  if (req.params.id) {
    id = req.params.id;
  }
  try {
    if (getAll) {
      const posts = (await PostModel.find().sort({ timestamp: -1 })).filter(
        (post) => post.author !== req.user.id
      );
      res.status(200).json(posts);
    } else {
      const posts = await PostModel.find({ author: id }).sort({
        timestamp: -1,
      });
      const users = await UserModel.find();
      const newposts = posts.map((post) => {
        post.comments = post.comments.map((comment) => {
          const user = users.find(
            (user) => user._id.toString() === comment.author
          );
          comment.avatarUrl = user.avatar;
          comment.authorFirstName = user.firstName;
          comment.authorLastName = user.lastName;
          return comment;
        });
        return post;
      });
      res.status(200).json(newposts);
    }
  } catch (error) {
    errorHandler(res, error);
  }
};

export const getPostById = async (req, res) => {
  try {
    const post = await PostModel.findById(req.params.id);
    res.status(200).json(post);
  } catch (error) {
    errorHandler(res, error);
  }
};

export const deletePost = async (req, res) => {
  try {
    await PostModel.findByIdAndDelete({ _id: req.params.id });
    res.status(200).json({ message: "Succesfully removed" });
  } catch (error) {
    errorHandler(res, error);
  }
};

export const createPost = async (req, res) => {
  try {
    const post = await new PostModel({
      author: req.user.id,
      content: req.body.content,
      timestamp: new Date(),
      comments: [],
      likes: [],
    }).save();

    res.status(201).json(post);
  } catch (error) {
    errorHandler(res, error);
  }
};

export const updatePost = async (req, res) => {
  try {
    const updatedPost = await PostModel.findByIdAndUpdate(
      { _id: req.params.id },
      { $set: req.body },
      { new: true }
    );
    res.status(200).json(updatedPost);
  } catch (error) {
    errorHandler(res, error);
  }
};

export const addComment = async (req, res) => {
  try {
    const user = await UserModel.findById(req.body.author);
    const newComment = {
      author: req.body.author,
      content: req.body.content,
      timestamp: new Date(),
      avatarUrl: user.avatar,
      authorFirstName: user.firstName,
      authorLastName: user.lastName,
      _id: uuidv4(),
    };
    const post = await PostModel.findByIdAndUpdate(
      { _id: req.body.postId },
      { $push: { comments: newComment } },
      { new: true }
    );
    res.status(201).json(newComment);
  } catch (error) {
    errorHandler(res, error);
  }
};

export const deleteComment = async (req, res) => {
  try {
    const post = await PostModel.findByIdAndUpdate(
      { _id: req.query.postId },
      { $pull: { comments: { _id: req.params.id } } }
    );
    res.status(200).json({ message: "Succesfully removed" });
  } catch (error) {
    errorHandler(res, error);
  }
};
