import { MessageModel } from "./../models/message.js";
import { errorHandler } from "../utils/errorHandler.js";

export const getAllMessages = async (req, res) => {
  const conditions = {
    $or: [{ recipient: req.user.id }, { sender: req.user.id }],
  };
  try {
    const messages = await MessageModel.find(conditions).sort({
      timestamp: 1,
    });
    res.status(200).json(messages);
  } catch (error) {
    errorHandler(res, error);
  }
};

export const createMessage = async (req, res) => {
  try {
    const message = await new MessageModel({
      sender: req.user.id,
      recipient: req.body.recipient,
      content: req.body.content,
      timestamp: new Date(),
      status: "new",
    }).save();

    res.status(201).json(message);
  } catch (error) {
    errorHandler(res, error);
  }
};

export const updateMessage = async (req, res) => {
  try {
    const updatedMessage = await MessageModel.findByIdAndUpdate(
      { _id: req.params.id },
      { $set: req.body },
      { new: true }
    );
    res.status(200).json(updatedMessage);
  } catch (error) {
    errorHandler(res, error);
  }
};

export const getMessagesByUserId = async (req, res) => {
  const conditions = {
    $or: [{ recipient: req.user.id }, { sender: req.user.id }],
  };
  try {
    let messages = await MessageModel.find(conditions).sort({
      timestamp: 1,
    });
    messages = messages.filter((message) => {
      if (
        message.sender === req.params.id ||
        message.recipient === req.params.id
      ) {
        return true;
      }
      return false;
    });
    res.status(200).json(messages);
  } catch (error) {
    errorHandler(res, error);
  }
};

export const maskMessagesAsRead = async (req, res) => {
  try {
    const result = await MessageModel.updateMany(
      { _id: { $in: req.body } },
      { $set: { status: "read" } },
      { multi: true }
    );
    res.status(200).json({ message: "Messages was read" });
  } catch (error) {
    errorHandler(res, error);
  }
};
