import bcrypt from "bcryptjs";
import { UserModel } from "../models/user.js";
import { errorHandler } from "./../utils/errorHandler.js";

export const getAllUsers = async (req, res) => {
  const limit = req.query.limit;
  const isFriend = req.query.isFriend;
  const userId = req.query.userId;
  try {
    if (isFriend && userId) {
      const user = await UserModel.find({ _id: userId });
      const users = await UserModel.find({ _id: { $in: user[0].friends } });
      res.status(200).json(users);
    } else {
      const users = await UserModel.find();
      res.status(200).json(users);
    }
  } catch (error) {
    errorHandler(res, error);
  }
};

export const updateCurrentUser = async (req, res) => {
  const salt = bcrypt.genSaltSync(10);
  let candidate = JSON.parse(req.body.user);
  const avatar = req.file ? req.file.filename : "";
  if (avatar) {
    candidate.avatar = "uploads/" + avatar;
  }
  console.log(candidate);

  try {
    if (req.body.currentPassword) {
      const user = await UserModel.findById(req.user.id);
      const isPasswordsMatched = bcrypt.compareSync(
        candidate.currentPassword,
        user.password
      );
      if (isPasswordsMatched) {
        candidate = {
          password: bcrypt.hashSync(candidate.newPassword, salt),
          email: candidate.email,
          isShowProfile: candidate.isShowProfile,
        };
      } else {
        res.status(400).json("Current password is invalid");
      }
    }
    const updatedUser = await UserModel.findByIdAndUpdate(
      { _id: req.user.id },
      { $set: candidate },
      { new: true }
    );

    res.status(200).json(updatedUser);
  } catch (error) {
    errorHandler(res, error);
  }
};

export const getCurrentUser = async (req, res) => {
  try {
    const user = await UserModel.findById(req.user.id);
    res.status(200).json(user);
  } catch (error) {
    errorHandler(res, error);
  }
};

export const getUserById = async (req, res) => {
  try {
    const user = await UserModel.findById(req.params.id);
    res.status(200).json(user);
  } catch (error) {
    errorHandler(res, error);
  }
};

export const addToFriend = async (req, res) => {
  try {
    const currentUser = await UserModel.findByIdAndUpdate(
      { _id: req.user.id },
      { $push: { friends: req.body._id } },
      { new: true }
    );
    const friend = await UserModel.findByIdAndUpdate(
      { _id: req.body._id },
      { $push: { friends: req.user.id } },
      { new: true }
    );

    res.status(201).json({ message: "Succesfully added" });
  } catch (error) {
    errorHandler(res, error);
  }
};

export const deleteFriend = async (req, res) => {
  try {
    const currentUser = await UserModel.findByIdAndUpdate(
      { _id: req.user.id },
      { $pull: { friends: req.params.id } },
      { new: true }
    );
    const friend = await UserModel.findByIdAndUpdate(
      { _id: req.params.id },
      { $pull: { friends: req.user.id } },
      { new: true }
    );
    res.status(201).json({ message: "Succesfully removed" });
  } catch (error) {
    errorHandler(res, error);
  }
};
