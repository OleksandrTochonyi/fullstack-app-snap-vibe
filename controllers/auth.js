import bcrypt from "bcryptjs";
import jwt from "jsonwebtoken";

import { UserModel } from "../models/user.js";
import { jwtSecretKey } from "../config/keys.js";
import { errorHandler } from "./../utils/errorHandler.js";

export const login = async (req, res) => {
  const { email, password } = req.body;
  const candidate = await UserModel.findOne({ email });

  // If Email is not found
  if (!candidate) {
    return res.status(404).json({ message: "Email is not found" });
  }

  // Email is exist
  const isPasswordsMatched = bcrypt.compareSync(password, candidate.password);
  // Passwords isn't matched
  if (!isPasswordsMatched) {
    return res.status(401).json({ message: "Incorrect password" });
  }

  // Passwords is matched
  const token = jwt.sign(
    {
      email: candidate.email,
      userId: candidate._id,
    },
    jwtSecretKey,
    {
      expiresIn: 3600,
    }
  );
  res.status(200).json({
    token: `Bearer ${token}`,
    expiresIn: 3600,
    user: candidate
  });
};

export const register = async (req, res) => { 

  const regUser = JSON.parse(req.body.user) 
  const candidate = await UserModel.findOne({ email: regUser.email });
  if (candidate) {
    // User already registered
    res.status(409).json({ message: "Email is already exist" });
    return;
  }
  const avatar = req.file ? req.file.filename : "";
  const salt = bcrypt.genSaltSync(10);
  const { email, password, firstName, lastName } = regUser;
  const user = new UserModel({
    email,
    password: bcrypt.hashSync(password, salt),
    firstName,
    lastName,
    about: regUser.about,
    isShowProfile: true,
  });
  if (avatar) {
    user.avatar = "uploads/" + avatar;
  }
  try {
    await user.save();
    res.status(201).json(user);
  } catch (error) {
    errorHandler(res, error);
  }
};
