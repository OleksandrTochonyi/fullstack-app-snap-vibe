import { Strategy as JwtStrategy, ExtractJwt } from "passport-jwt";
import passport from "passport";

import { jwtSecretKey } from "../config/keys.js";
import { UserModel } from "../models/user.js";

const opts = {
  jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
  secretOrKey: jwtSecretKey,
};

export const passportJwt = () => {
  passport.use(
    new JwtStrategy(opts, async (payload, done) => {
      try {
        const user = await UserModel.findById(payload.userId).select(
          "email id"
        );
        if (user) {
          done(null, user);
        } else {
          done(null, false);
        }
      } catch (error) {
        // ERROR HANDLER WILL BE HERE
        console.log(error);
      }
    })
  );
};
