interface IAboutUser {
  city: string;
  country: string;
  mobileNumber: string;
  languages?: string[];
}

export interface IUser {
  _id?: string;
  firstName?: string;
  lastName?: string;
  dateOfBirthday?: Date;
  email?: string;
  password?: string;
  friends?: string[];
  about?: IAboutUser;
  avatar?: string;
  isShowProfile?: boolean;
}

export interface IComment {
  author: string;
  timestamp?: Date;
  content?: string;
  _id: string;
}

export interface IPost {
  _id: string;
  author: string;
  content: string;
  timestamp?: Date;
  comments?: IComment[];
  likes?: string[];
}

export interface IMessage {
  _id?: string;
  sender?: string;
  recipient?: string;
  timestamp?: Date;
  content: string;
  status?: string;
}

export interface IAlert {
  type: string;
  text: string;
}
