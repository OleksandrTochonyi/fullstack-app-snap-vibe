import {
  AfterViewInit,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { FloatingActionButton } from 'materialize-css';
import Quill from 'quill';
import BlotFormatter from 'quill-blot-formatter';
import 'quill-emoji/dist/quill-emoji.js';

import { getQuillsModules } from 'src/app/shared/helpers/quill-editor.model';
import { IPost, IUser } from 'src/app/shared/interfaces';
import { MaterializeService } from 'src/app/shared/services/materialize.service';
import { PostsService } from 'src/app/shared/services/posts.service';
import { UserService } from '../../services/user.service';
import { AlertService } from '../../services/alert.service';

Quill.register('modules/blotFormatter', BlotFormatter);

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.scss'],
})
export class PostComponent implements OnInit, AfterViewInit {
  modules = {};
  isShowComments: boolean = false;
  //isCurrentUser: boolean = true;
  form: FormGroup;
  @Input() post: IPost;
  @Input() user: IUser;
  @Input() isCurrentUser: boolean = true;
  @Output() onDelete: EventEmitter<any> = new EventEmitter<any>();
  @Output() onEdit: EventEmitter<any> = new EventEmitter<any>();
  @ViewChild('modal') modalRef: ElementRef;
  modal: any;

  constructor(
    private route: ActivatedRoute,
    private elementRef: ElementRef,
    private userService: UserService,
    private postService: PostsService,
    private router: Router,
    private alertService: AlertService,
  ) {}

  ngOnInit(): void {
    this.initMaterializeButton();
    this.modules = getQuillsModules();
    this.form = new FormGroup({
      content: new FormControl('', Validators.required),
    });
    this.route.params.subscribe((params: Params) => {
      if (params['id'] && params['id'] !== this.userService.getUser()['_id']) {
        this.isCurrentUser = false;
      }
    });
    this.userService.getUserById(this.post.author).subscribe((resp) => {
      if (!this.user) {
        this.user = resp;
      }
    });
  }

  ngAfterViewInit(): void {
    this.modal = MaterializeService.initModal(this.modalRef);
  }

  toggleComments(): void {
    this.isShowComments = !this.isShowComments;
  }

  openModal() {
    this.modal.open();
  }

  goToProfile() {
    this.router.navigate(['main/profile', this.post.author]);
  }

  deletePost() {
    this.onDelete.next(this.post['_id']);
  }

  deleteComment(id: any) {
    this.postService.removeComment(this.post['_id'], id).subscribe(
      (resp) => {
        this.post.comments = this.post.comments.filter(
          (comment) => comment['_id'] !== id
        );
        this.alertService.warning('Comment deleted!')
      },
      (error) => this.alertService.warning(error.message)
    );
  }

  editPost() {
    this.onEdit.next(this.post['_id']);
  }

  initMaterializeButton(): void {
    const actionButtonMaterialize =
      this.elementRef.nativeElement.querySelector('.fixed-action-btn');
    FloatingActionButton.init(actionButtonMaterialize, {
      direction: 'left',
      hoverEnabled: false,
    });
  }

  addComment() {
    const newComment = {
      author: this.userService.getUser()['_id'],
      content: this.form.value.content,
      timestamp: new Date(),
    };
    this.postService.addComment(this.post['_id'], newComment).subscribe(
      (resp) => {
        this.post.comments.push(resp);
        this.form.reset();
        this.alertService.success('Comment added!')
      },
      (error) => {
        this.alertService.warning(error.message)
      }
    );
  }
}
