import { Component, ElementRef, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FloatingActionButton } from 'materialize-css';
import { Router } from '@angular/router';

import { UserService } from '../../services/user.service';


@Component({
  selector: 'app-comment',
  templateUrl: './comment.component.html',
  styleUrls: ['./comment.component.scss'],
})
export class CommentComponent implements OnInit {
  @Input() comment: any;
  @Output() onDelete: EventEmitter<any> = new EventEmitter<any>();

  constructor(
    private elementRef: ElementRef,
    private router: Router,
    private userService: UserService
  ) {}

  ngOnInit(): void {
    this.initMaterializeButton();
  }

  initMaterializeButton(): void {
    const actionButtonMaterialize =
      this.elementRef.nativeElement.querySelectorAll('.fixed-action-btn');
    FloatingActionButton.init(actionButtonMaterialize, {
      direction: 'left',
      hoverEnabled: false,
    });
  }
  goToProfile() {
    const currentUserId = this.userService.getUser()['_id'].toString();
    const profileToRedirectId = this.comment.author;
    if (profileToRedirectId === currentUserId) {
      this.router.navigate(['main/profile']);
    }
    this.router.navigate(['main/profile', this.comment.author]);
  }

  isCommentAuthor(id: string): boolean {
    if (id === this.userService.getUser()['_id']) {
      return true;
    }
    return false;
  }

  removeComment() {    
    this.onDelete.next(this.comment['_id']);
  }
}
