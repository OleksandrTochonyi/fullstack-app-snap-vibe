import { Component,  OnInit } from '@angular/core';

import { UserService } from './../../services/user.service';
import { IUser } from '../../interfaces';
import { AuthService } from './../../services/auth.service';

@Component({
  selector: 'app-main-layout',
  templateUrl: './main-layout.component.html',
  styleUrls: ['./main-layout.component.scss'],
})
export class MainLayoutComponent implements OnInit {
  user: IUser;

  constructor(
    private userService: UserService,
    private authService: AuthService
  ) {}

  ngOnInit(): void {
    this.user = this.userService.getUser();
  }

  logout() {
    this.authService.logout();
  }
}
