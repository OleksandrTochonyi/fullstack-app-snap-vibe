import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, tap } from 'rxjs';
import { IUser } from '../interfaces';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  private user: IUser;

  constructor(private http: HttpClient) {}

  getCurrentUser(): Observable<IUser> {
    return this.http.get<IUser>('api/user');
  }

  getAllUsers(
    limit: any = null,
    isFriend: boolean = false,
    userId: string = ''
  ): Observable<IUser[]> {
    let queryParams = '';
    if (limit) {
      queryParams += `limit=${limit}`;
      queryParams += '&';
    }
    if (isFriend) {
      queryParams += `isFriend=${isFriend}`;
      queryParams += '&';
    }
    if (userId) {
      queryParams += `userId=${userId}`;
    }
    return this.http.get<IUser[]>(`api/users?${queryParams}`);
  }

  updateCurrentUser(user: IUser, image?: File): Observable<IUser> {
    const fd = new FormData();
    if (image) {
      fd.append('image', image, image.name);
    }
    fd.append('user', JSON.stringify(user));    
    return this.http.patch<IUser>('api/users', fd).pipe(
      tap((resp) => {
        this.setUser(resp);
        localStorage.setItem('user', JSON.stringify(resp));
      })
    );
  }

  addToFriends(id: string): Observable<IUser> {
    return this.http.patch<IUser>('api/user', { _id: id });
  }

  deleteFriend(id: string): Observable<IUser> {
    return this.http.delete<IUser>(`api/users/${id}`);
  }

  setUser(user: IUser) {
    this.user = user;
  }

  getUser() {
    return this.user;
  }

  getUserById(id: string): Observable<IUser> {
    return this.http.get<IUser>(`api/users/${id}`);
  }
}
