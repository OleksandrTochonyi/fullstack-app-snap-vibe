import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { IPost } from '../interfaces';
import { HttpClient, HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class PostsService {
  constructor(private http: HttpClient) {}

  getAll(getAll?: boolean): Observable<IPost[]> {
    let queryParams = '';
    if (getAll) {
      queryParams = `getAll=true`;
    }
    return this.http.get<IPost[]>(`api/posts?${queryParams}`);
  }

  getAllPostsByUserId(id: string): Observable<IPost[]> {
    return this.http.get<IPost[]>(`api/posts/${id}`);
  }

  getPostById(id: string): Observable<IPost> {
    return this.http.get<IPost>(`api/post/${id}`);
  }

  createPost(post: IPost): Observable<IPost> {
    return this.http.post<IPost>('api/posts', post);
  }

  deletePost(id: string): Observable<any> {
    return this.http.delete<any>(`api/posts/${id}`);
  }

  updatePost(post: IPost): Observable<IPost> {
    return this.http.patch<IPost>(`api/posts/${post['_id']}`, post);
  }

  addComment(postId: string, comment: any): Observable<any> {
    return this.http.post('api/posts/comment', { ...comment, postId });
  }
  removeComment(postId: string, commentId: string): Observable<any> {
    let params = new HttpParams();
    params = params.append('postId', postId);
    return this.http.delete(`api/posts/comment/${commentId}`, { params });
  }
}
