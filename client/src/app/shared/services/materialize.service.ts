import { ElementRef, Injectable } from '@angular/core';
import {
  Modal,
  Tabs,
  Datepicker,
  Tooltip,
  Parallax,
  FormSelect,
} from 'materialize-css';

@Injectable({
  providedIn: 'root',
})
export class MaterializeService {
  static initModal(ref: ElementRef) {
    if (ref && ref.nativeElement) {
      return Modal.init(ref.nativeElement);
    }
    return undefined;
  }

  initTabs(element: ElementRef) {
    const tabsElement = element.nativeElement;
    return Tabs.init(tabsElement, {});
  }

  initDatepicker(element: ElementRef, options: any) {
    const datepickerElement = element.nativeElement;
    Datepicker.init(datepickerElement, options);
  }

  initTooltip(element: ElementRef) {
    const tooltipElement = element.nativeElement;
    Tooltip.init(tooltipElement);
  }

  initParalax(element: ElementRef) {
    const parallaxElement = element.nativeElement;
    Parallax.init(parallaxElement);
  }

  intitSelect(element: ElementRef) {
    const selectElement = element.nativeElement;
    FormSelect.init(selectElement);
  }
}
