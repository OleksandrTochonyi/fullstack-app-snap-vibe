import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject, tap } from 'rxjs';
import { IMessage } from '../interfaces';

@Injectable({
  providedIn: 'root',
})
export class MessagesService {
  constructor(private http: HttpClient) {}

  getAll(): Observable<IMessage[]> {
    return this.http.get<IMessage[]>(`api/messages`);
  }

  createMessage(message: IMessage): Observable<IMessage> {
    return this.http.post<IMessage>('api/messages', message);
  }

  getMessagesById(id: string): Observable<IMessage[]> {
    return this.http.get<IMessage[]>(`api/messages/${id}`)
  }

  markMessageAsRead(messagesIdArr: string[]):Observable<any>{
    return this.http.patch('api/messages', messagesIdArr)
  }
}
