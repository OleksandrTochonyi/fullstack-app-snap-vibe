import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, tap } from 'rxjs';
import { IUser } from '../interfaces';
import { Router } from '@angular/router';
import { UserService } from './user.service';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  private token: string;

  constructor(
    private http: HttpClient,
    private router: Router,
    private userService: UserService
  ) {}

  login(user: IUser): Observable<{ token: string; user: IUser }> {
    return this.http
      .post<{ token: string; user: IUser; expiresIn: string }>(
        '/api/auth/login',
        user
      )
      .pipe(
        tap(({ token, user, expiresIn }) => {
          const expDate = new Date(new Date().getTime() + +expiresIn * 1000);
          localStorage.setItem('auth-token-expires-in', expDate.toString());
          localStorage.setItem('auth-token', token);
          localStorage.setItem('user', JSON.stringify(user));
          this.userService.setUser(user);
          this.setToken(token);
        })
      );
  }

  register(user: IUser, image: any): Observable<IUser> {
    const fd = new FormData();
    if (image) {
      fd.append('image', image, image.name);
    }
    fd.append('user', JSON.stringify(user));
    console.log(user);
    console.log(image);
    return this.http.post<IUser>('/api/auth/register', fd);
  }

  setToken(token: string) {
    this.token = token;
  }

  getToken(): string {
    return this.token;
  }

  isTokenExpired(): boolean {
    const expTokenDate = localStorage.getItem('auth-token-expires-in');
    const isExpired = Date.now() > Date.parse(expTokenDate);
   
    if (!expTokenDate || isExpired) {
      this.logout()
      return true;
    }
    return false;
  }

  isAuthentificated(): boolean {
    return !!this.token;
  }

  logout() {
    this.setToken(null);
    localStorage.clear();
    this.router.navigate(['/login']);
  }
}
