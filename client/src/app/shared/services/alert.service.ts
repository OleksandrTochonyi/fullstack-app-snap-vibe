import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { IAlert } from '../interfaces';

@Injectable({
  providedIn: 'root',
})
export class AlertService {
  alert$ = new Subject<IAlert>();

  constructor() {}

  success(text: string) {
    this.alert$.next({
      type: 'success',
      text,
    });
  }
  warning(text: string) {
    this.alert$.next({
      type: 'warning',
      text,
    });
  }
  error(text: string) {
    this.alert$.next({
      type: 'danger',
      text,
    });
  }

  info(text: string) {
    this.alert$.next({
      type: 'info',
      text,
    });
  }
}
