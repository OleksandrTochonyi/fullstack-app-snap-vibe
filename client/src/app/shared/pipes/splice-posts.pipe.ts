import { Pipe, PipeTransform } from '@angular/core';
import { IPost } from 'src/app/shared/interfaces';

@Pipe({
  name: 'spliceposts',
})
export class SplicepostsPipe implements PipeTransform {
  transform(posts: IPost[], quantity: string | number): IPost[] {
    if (!quantity) {
      return posts;
    } else {
      return posts.slice(0, +quantity);
    }
  }
}
