import { Pipe, PipeTransform } from '@angular/core';
import { IUser } from '../interfaces';
import { ageCalculator } from '../helpers/ageCalculator';

@Pipe({
  name: 'filterbyage',
})
export class FilterbyagePipe implements PipeTransform {
  transform(users: IUser[], from: string, to: string): IUser[] {
    if (!from && !to) {
      return users;
    } else {
      const filteredUsers = users.filter((user) => {
        if (!!user.dateOfBirthday) {
          const startAge = from ? from : 0;
          const endAge = to ? to : 1000;
          const dateOfBirthday = ageCalculator(user.dateOfBirthday);
          if (+dateOfBirthday >= +startAge && +dateOfBirthday <= +endAge) {
            
            return true;
          } else return false;
        } else {
          return true;
        }
      });
      return filteredUsers;
    }
  }
}
