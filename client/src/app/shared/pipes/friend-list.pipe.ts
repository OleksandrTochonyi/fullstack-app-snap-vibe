import { Pipe, PipeTransform } from '@angular/core';
import { IUser } from '../interfaces';

@Pipe({
  name: 'friendlist',
})
export class FriendlistPipe implements PipeTransform {
  transform(users: IUser[], currentUserId: string): IUser[] {
    if (users && currentUserId) {
      return users.filter((user) => user.friends.includes(currentUserId));
    }
    return users;
  }
}
