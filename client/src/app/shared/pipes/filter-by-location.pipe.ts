import { Pipe, PipeTransform } from '@angular/core';
import { IUser } from './../interfaces';

@Pipe({
  name: 'filterByLocation',
})
export class FilterByLocationPipe implements PipeTransform {
  transform(users: IUser[], country: string, city: string): IUser[] {
    let filteredUsers = users;
    if (country !== 'All') {
      filteredUsers = filteredUsers.filter(
        (user) => user.about.country.toLowerCase() === country.toLowerCase()
      );
    }
    if (city !== 'All') {
      filteredUsers = filteredUsers.filter(
        (user) => user.about.city.toLowerCase() === city.toLowerCase()
      );
    }
    return filteredUsers;
  }
}
