import { Pipe, PipeTransform } from '@angular/core';
import { IPost } from '../interfaces';

@Pipe({
  name: 'searchPostByContent',
})
export class SearchPostByNamePipe implements PipeTransform {
  transform(posts: IPost[], searchValue: string): IPost[] {
    if (!searchValue.trim()) {
      return posts;
    }

    return posts.filter((post) =>
      post.content.toLowerCase().includes(searchValue.toLowerCase().trim())
    );
  }
}
