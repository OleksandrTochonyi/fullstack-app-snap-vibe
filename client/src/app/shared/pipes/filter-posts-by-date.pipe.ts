import { Pipe, PipeTransform } from '@angular/core';
import { IPost } from 'src/app/shared/interfaces';

@Pipe({
  name: 'filterPostsByDate',
})
export class FilterPostsByDatePipe implements PipeTransform {
  transform(posts: IPost[], date: any): IPost[] {
    if (!date) {
      return posts;
    }
    return posts.filter((post) => new Date(post.timestamp) > new Date(date));
  }
}
