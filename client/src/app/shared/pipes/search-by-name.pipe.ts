import { Pipe, PipeTransform } from '@angular/core';
import { IUser } from 'src/app/shared/interfaces';

@Pipe({
  name: 'searchByName',
})
export class SearchByNamePipe implements PipeTransform {
  transform(users: IUser[], searchValue: string): IUser[] {
    if (!searchValue.trim()) {
      return users;
    }
    return users.filter(user=> user.firstName.toLowerCase().includes(searchValue.toLowerCase()) || user.lastName.toLowerCase().includes(searchValue.toLowerCase()))
  }
}
