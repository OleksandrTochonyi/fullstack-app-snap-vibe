export const getQuillsModules = (value?: string) => {
  const modules = {
    'emoji-shortname': true,
    'emoji-textarea': false,
    'emoji-toolbar': true,
    blotFormatter: {},
    toolbar: {
      container: [
        ['bold', 'italic', 'underline', 'strike'],
        ['blockquote', 'code-block'],

        [{ header: 1 }, { header: 2 }],
        [{ list: 'ordered' }, { list: 'bullet' }],
        [{ size: ['small', false, 'large', 'huge'] }], 
        [{ color: [] as any }, { background: [] as any }],
        [{ font: [] as any }],
        [{ align: [] as any }],

        ['emoji'],
        ['link', value],
        
      ],
      handlers: { emoji: function () {} },
    },
  };
  return modules;
};
