import {
  AfterViewInit,
  Component,
  ElementRef,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { updateTextFields } from 'materialize-css';

import { UserService } from '../shared/services/user.service';
import { MaterializeService } from '../shared/services/materialize.service';
import { ConfirmPasswordValidator } from '../shared/helpers/custom-validators';
import { AlertService } from '../shared/services/alert.service';

@Component({
  selector: 'app-edit-profile-page',
  templateUrl: './edit-profile-page.component.html',
  styleUrls: ['./edit-profile-page.component.scss'],
})
export class EditProfilePageComponent
  implements OnInit, AfterViewInit, OnDestroy
{
  form: FormGroup;
  avatar: File;
  currentAvatar: string;
  avatarPreview: any = '';
  selectedBirthdayDate: any;
  userSub: Subscription;
  @ViewChild('datepicker') datepickerRef: ElementRef;
  @ViewChild('tabs') tabsRef: ElementRef;
  @ViewChild('input') inputRef: ElementRef;

  constructor(
    private userService: UserService,
    private router: Router,
    private matService: MaterializeService,
    private formBuilder: FormBuilder,
    private alertService: AlertService
  ) {}

  ngOnInit(): void {
    this.form = this.formBuilder.group(
      {
        email: new FormControl('', [Validators.required, Validators.email]),
        password: new FormControl('', [Validators.minLength(8)]),
        confirmPassword: new FormControl('', [Validators.minLength(8)]),
        currentPassword: new FormControl('', [Validators.minLength(8)]),
        firstName: new FormControl('', Validators.required),
        lastName: new FormControl('', Validators.required),
        country: new FormControl('', Validators.required),
        city: new FormControl('', Validators.required),
        mobileNumber: new FormControl(''),
        dateOfBirthday: new FormControl(''),
        isShowProfile: new FormControl(true),
      },
      {
        validator: ConfirmPasswordValidator('password', 'confirmPassword'),
      }
    );
    this.form.disable();
    this.userSub = this.userService.getCurrentUser().subscribe(
      (resp) => {
        this.currentAvatar = resp.avatar;
        this.form.patchValue({
          firstName: resp.firstName,
          lastName: resp.lastName,
          country: resp.about.country,
          city: resp.about.city,
          mobileNumber: resp.about.mobileNumber || null,
          email: resp.email,
          isShowProfile: resp.isShowProfile,
        });
        updateTextFields();
        this.selectedBirthdayDate = new Date(resp.dateOfBirthday);
        this.initDatepicker();
      },
      (error) => console.log(error),
      () => {
        this.form.enable();
      }
    );
  }

  ngAfterViewInit(): void {
    this.matService.initTabs(this.tabsRef);
  }

  ngOnDestroy(): void {
    if (this.userSub) {
      this.userSub.unsubscribe();
    }
  }

  triggerClickInput() {
    this.inputRef.nativeElement.click();
  }

  onFileUpload(event: any) {
    const file = event.target.files[0];
    this.avatar = file;

    const reader = new FileReader();

    reader.onload = () => {
      this.avatarPreview = reader.result;
    };

    reader.readAsDataURL(file);
  }

  initDatepicker() {
    const options = {
      onClose: () => {
        const selectedDateElement: any = document.querySelector('.datepicker');
        if (selectedDateElement) {
          const selectedDateValue = selectedDateElement.value;
          this.selectedBirthdayDate = new Date(selectedDateValue);
        }
      },
      defaultDate: this.selectedBirthdayDate,
      setDefaultDate: true,
    };
    this.matService.initDatepicker(this.datepickerRef, options);
  }

  updateUser() {
    const updatedUser = {
      firstName: this.form.value.firstName,
      lastName: this.form.value.lastName,
      dateOfBirthday: this.selectedBirthdayDate,
      about: {
        country: this.form.value.country,
        city: this.form.value.city,
        mobileNumber: this.form.value.mobileNumber,
      },
    };
    if (this.form.valid) {
      let avatar = null;
      if (this.avatar) {
        avatar = this.avatar;
      }
      this.userService.updateCurrentUser(updatedUser, avatar).subscribe(
        (resp) => {
          this.alertService.info('Profile updated');
        },
        (error) => this.alertService.info(error.message)
      );
    }
  }

  cancelChanges() {
    this.form.reset();
    this.router.navigate(['main', 'profile']);
  }

  updatePrivateInfo() {
    const privateInfoToUpdate = {
      email: this.form.value.email,
      isShowProfile: this.form.value.isShowProfile,
      currentPassword: this.form.value.currentPassword,
      newPassword: this.form.value.password,
    };

    if (this.form.touched && this.form.valid) {
      const currentPassword = this.form.value.currentPassword;
      const newPassword = this.form.value.password;
      if (!!currentPassword.trim() && !!newPassword.trim()) {
        this.userService
          .updateCurrentUser(privateInfoToUpdate)
          .subscribe((resp) => {
            console.log('private info - updated');
          });
        this.form.patchValue({
          currentPassword: '',
          password: '',
          confirmPassword: '',
        });
        updateTextFields();
      } else {
        const updateEmailAndIsShow = {
          email: this.form.value.email,
          isShowProfile: this.form.value.isShowProfile,
        };
        this.userService.updateCurrentUser(updateEmailAndIsShow).subscribe(
          (resp) => {
            this.alertService.info('Private info updated');
          },
          (error) => this.alertService.info(error.message)
        );
      }
    }
  }

  isFormChange(): boolean {
    return !!this.avatar || this.form.dirty || this.form.invalid;
  }
}
