import {
  AfterViewInit,
  Component,
  ElementRef,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core';
import { Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';

import { UserService } from './../shared/services/user.service';
import { IUser } from '../shared/interfaces';
import { MaterializeService } from '../shared/services/materialize.service';
import { ageCalculator } from '../shared/helpers/ageCalculator';
import { AlertService } from '../shared/services/alert.service';

@Component({
  selector: 'app-people-page',
  templateUrl: './people-page.component.html',
  styleUrls: ['./people-page.component.scss'],
})
export class PeoplePageComponent implements OnInit, AfterViewInit, OnDestroy {
  users: IUser[];
  usersSub: Subscription;
  currentUserId: string;
  searchValue: string = '';
  tabs: any;
  selectedCountry: string = 'All';
  selectedCity: string = 'All';
  ageFrom: string = '';
  ageTo: string = '';
  countries: string[] = ['All'];
  cities: string[] = ['All'];
  selectRefSub: Subscription;
  searchByCountry: string = '';
  displayedUsers: IUser[];
  displayedFriend: IUser[];
  pages: number[];
  currentPage: number = 1;

  @ViewChild('tabs') tabsRef: ElementRef;
  @ViewChild('select', { static: false }) selectRef: ElementRef;

  constructor(
    private userService: UserService,
    private materializeService: MaterializeService,
    private route: ActivatedRoute,
    private alertService: AlertService
  ) {}

  ngOnInit(): void {
    this.currentUserId = this.userService.getUser()._id;
    this.usersSub = this.userService.getAllUsers().subscribe(
      (resp) => {
        this.users = resp.filter((user) => user._id !== this.currentUserId);
        this.getCounties(this.users);
        this.getCities(this.users);
        this.updateDisplayedUsers();
      },
      (error) =>this.alertService.error(error.message)
    );
  }

  ngAfterViewInit(): void {
    this.tabs = this.materializeService.initTabs(this.tabsRef);
    this.route.queryParams.subscribe((params) => {
      if (params['findFriend']) {
        this.tabs.select('people');
      } else {
        this.tabs.select('friends');
      }
    });
  }

  ngOnDestroy(): void {
    if (this.usersSub) {
      this.usersSub.unsubscribe();
    }
    if (this.selectRefSub) {
      this.selectRefSub.unsubscribe();
    }
  }

  onAddFriend(id: string) {
    this.userService.addToFriends(id).subscribe((resp) => {
      this.users = this.users.map((user) => {
        if (user._id === id) {
          user.friends.push(this.userService.getUser()._id);
          return user;
        }
        return user;
      });
      this.alertService.success('Friend added!')
    },
    (error) =>this.alertService.error(error.message));
  }

  getCounties(users: IUser[]) {
    const countries = [...new Set(users.map((user) => user.about.country))];
    this.countries = [...this.countries, ...countries];
  }

  getCities(users: IUser[]) {
    const cities = [...new Set(users.map((user) => user.about.city))];
    this.cities = [...this.cities, ...cities];
  }

  onDeleteFriend(id: string) {
    this.userService.deleteFriend(id).subscribe((resp) => {
      this.users = this.users.map((user) => {
        if (user._id === id) {
          user.friends = user.friends.filter(
            (id) => id !== this.userService.getUser()._id
          );
          return user;
        }
        return user;
      });
      this.alertService.warning('Friend deleted!')
    },
    (error) =>this.alertService.error(error.message));
  }

  onPageChange(page: any) {
    this.currentPage = page;
    // Perform any necessary actions when the page changes
  }

  updateDisplayedUsers(): void {
    const filteredUsers = this.filterUsers();
    const startIndex = (this.currentPage - 1) * 5;
    const endIndex = startIndex + 5;
    this.displayedUsers = filteredUsers.slice(startIndex, endIndex);

    if (filteredUsers.length <= 5) {
      this.pages = [1];
    } else {
      this.pages = Array(Math.ceil(this.users.length / 5))
        .fill(0)
        .map((x, i) => i + 1);
    }
  }

  goToPage(page: number): void {
    if (page >= 1 && page <= this.pages.length) {
      this.currentPage = page;
      this.updateDisplayedUsers();
    }
  }

  nextPage(): void {
    if (this.currentPage < this.pages.length) {
      this.currentPage++;
      this.updateDisplayedUsers();
    }
  }

  previousPage(): void {
    if (this.currentPage > 1) {
      this.currentPage--;
      this.updateDisplayedUsers();
    }
  }

  filterUsers(): IUser[] {
    let users = this.users;
    if (this.searchValue.trim() !== '') {
      users = users.filter(
        (user) =>
          user.firstName
            .toLowerCase()
            .includes(this.searchValue.toLowerCase()) ||
          user.lastName.toLowerCase().includes(this.searchValue.toLowerCase())
      );
    }
    if (this.selectedCountry !== 'All') {
      users = users.filter((user) =>
        user.about.country
          .toLowerCase()
          .includes(this.selectedCountry.toLowerCase())
      );
    }
    if (this.selectedCity !== 'All') {
      users = users.filter((user) =>
        user.about.city.toLowerCase().includes(this.selectedCity.toLowerCase())
      );
    }

    if (this.ageFrom || this.ageTo) {
      users = users.filter((user) => {
        if (!!user.dateOfBirthday) {
          const startAge = this.ageFrom ? this.ageFrom : 0;
          const endAge = this.ageTo ? this.ageTo : 1000;
          const dateOfBirthday = ageCalculator(user.dateOfBirthday);
          if (+dateOfBirthday >= +startAge && +dateOfBirthday <= +endAge) {
            return true;
          } else return false;
        } else {
          return true;
        }
      });
    }

    return users;
  }

  searchUsers() {
    this.currentPage = 1;
    this.updateDisplayedUsers();
  }
}
