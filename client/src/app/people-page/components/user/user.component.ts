import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';

import { IUser } from 'src/app/shared/interfaces';
import { UserService } from 'src/app/shared/services/user.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss'],
})
export class UserComponent implements OnInit {
  @Input() user: IUser;
  @Output() onAddFriend: EventEmitter<any> = new EventEmitter<any>();
  @Output() onDeleteFriend: EventEmitter<any> = new EventEmitter<any>();

  constructor(private userService: UserService, private router: Router) {}

  ngOnInit(): void {}

  isFriend() {
    return this.user.friends.includes(this.userService.getUser()._id);
  }

  goToProfile() {
    this.router.navigate(['main/profile', this.user['_id']]);
  }

  addToFriends() {
    this.onAddFriend.next(this.user._id)

  }

  removeFromFriends() {
    this.onDeleteFriend.next(this.user._id)
  }
  
  goToChat(id: string) {
    this.router.navigate(['main/messages', id]);
  }
}
