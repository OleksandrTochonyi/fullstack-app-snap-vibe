import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';

import { AuthService } from '../shared/services/auth.service';
import { AlertService } from '../shared/services/alert.service';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss'],
})
export class LoginPageComponent implements OnInit, OnDestroy {
  form: FormGroup;
  logSub: Subscription;
  constructor(
    private auth: AuthService,
    private router: Router,
    private alertService: AlertService
  ) {}

  ngOnInit(): void {
    this.form = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', [
        Validators.required,
        Validators.minLength(8),
      ]),
    });
  }

  ngOnDestroy(): void {
    if (this.logSub) {
      this.logSub.unsubscribe();
    }
  }

  login() {
    const email = this.form.value.email;
    const password = this.form.value.password;

    this.logSub = this.auth.login({ email, password }).subscribe(
      (resp) => {
        this.router.navigate(['main/profile']);
      },
      (error) => this.alertService.error(error.error.message)
    );
  }
}
