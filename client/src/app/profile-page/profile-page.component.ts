import {
  AfterViewInit,
  Component,
  ElementRef,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core';
import { UserService } from './../shared/services/user.service';
import { IPost, IUser } from '../shared/interfaces';
import { Subscription } from 'rxjs';
import { PostsService } from '../shared/services/posts.service';
import { getQuillsModules } from '../shared/helpers/quill-editor.model';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MaterializeService } from '../shared/services/materialize.service';
import { AlertService } from '../shared/services/alert.service';

@Component({
  selector: 'app-profile-page',
  templateUrl: './profile-page.component.html',
  styleUrls: ['./profile-page.component.scss'],
})
export class ProfilePageComponent implements OnInit, AfterViewInit, OnDestroy {
  modules = {};
  postsSub: Subscription;
  userSub: Subscription;
  user: IUser;
  isNew: boolean = true;
  postToEdit: IPost;
  posts: IPost[] = [];
  isCurrentUser = true;
  form: FormGroup;
  modal: any;
  isLoading: boolean = false;
  friendList: IUser[];
  friendsToShow: IUser[];

  @ViewChild('modal') modalRef: ElementRef;
  @ViewChild('parallax') parallaxRef: ElementRef;

  constructor(
    private userService: UserService,
    private postsService: PostsService,
    private route: ActivatedRoute,
    private router: Router,
    private matService: MaterializeService,
    private alertService: AlertService
  ) {}

  ngOnInit(): void {
    this.modules = getQuillsModules('image');
    //GET USER + POSTS by params or without params
    this.route.params.subscribe((params: Params) => {
      this.isLoading = true;
      if (params['id'] && params['id'] !== this.userService.getUser()['_id']) {
        this.isCurrentUser = false;
        this.userSub = this.userService.getUserById(params['id']).subscribe(
          (resp) => {
            this.user = resp;
            this.userService
              .getAllUsers(6, true, this.user._id)
              .subscribe((resp) => {
                this.friendList = resp;
                this.friendsToShow = this.setLimitAndRandomizeFriends(resp);
                this.isBlocked();
              });
          },
          (error) => {
            this.alertService.error(error.message);
          }
        );
        this.postsSub = this.postsService
          .getAllPostsByUserId(params['id'])
          .subscribe(
            (resp) => {
              this.posts = resp;
            },
            (error) => {
              this.alertService.error(error.message);
            },
            () => {
              this.isLoading = false;
            }
          );
      } else {
        this.isCurrentUser = true;
        this.user = this.userService.getUser();
        this.postsSub = this.postsService.getAll().subscribe(
          (resp) => {
            this.posts = resp;
          },
          (error) => {
            this.alertService.error(error.message);
          },
          () => {
            this.isLoading = false;
          }
        );
        this.userService
          .getAllUsers(6, true, this.user._id)
          .subscribe((resp) => {
            this.friendList = resp;
            this.friendsToShow = this.setLimitAndRandomizeFriends(resp);
          });
      }
    });
    //INIT FORM
    this.form = new FormGroup({
      content: new FormControl('', Validators.required),
    });
  }

  ngAfterViewInit(): void {
    this.modal = MaterializeService.initModal(this.modalRef);
    this.initPlarax();
  }

  ngOnDestroy(): void {
    if (this.postsSub) {
      this.postsSub.unsubscribe();
    }
    if (this.userSub) {
      this.userSub.unsubscribe();
    }
  }

  initPlarax() {
    this.matService.initParalax(this.parallaxRef);
  }

  savePost() {
    if (this.isNew) {
      this.postsService.createPost(this.form.value).subscribe((resp) => {
        this.posts.unshift(resp);
        this.form.reset();
        this.alertService.success('Post created!')
      }, error=> this.alertService.error(error.message));
    } else {
      const editedPost = this.posts.find(
        (post) => post['_id'] === this.postToEdit['_id']
      );
      if (editedPost.content === this.form.value.content) {
        this.closeEditMode();
        return;
      }
      this.postsService
        .updatePost({
          ...this.postToEdit,
          content: this.form.value.content,
        })
        .subscribe(
          (resp) => {
            this.posts = this.posts.map((post) => {
              if (post['_id'] === resp['_id']) {
                post.content = resp.content;
                return post;
              }
              return post;
            });
            this.alertService.info('Post edited!')
          },
          (error) => this.alertService.error(error.message),
          () => {
            this.form.reset();
          }
        );
    }
  }

  deletePost(id: string) {
    this.postsService.deletePost(id).subscribe((resp) => {
      this.posts = this.posts.filter((post) => post['_id'] !== id);
      this.alertService.warning('Post was deleted!')
    },
    (error) => this.alertService.error(error.message),);
  }

  editPost(id: any) {
    this.postsService.getPostById(id).subscribe((resp) => {
      this.form.patchValue({ content: resp.content });
      this.postToEdit = resp;
      this.isNew = false;
    },
    (error) => this.alertService.error(error.message));
  }

  closeEditMode() {
    this.isNew = true;
    this.form.reset();
  }

  isFriend(): boolean {
    return this.user.friends.includes(this.userService.getUser()._id);
  }

  addFriend() {
    this.userService.addToFriends(this.user._id).subscribe((resp) => {
      this.user.friends.push(this.userService.getUser()._id);
      this.alertService.success('Friend was added!')
    },
    (error) => this.alertService.error(error.message));
  }

  removeFriend() {
    this.userService.deleteFriend(this.user._id).subscribe((resp) => {
      this.user.friends = this.user.friends.filter(
        (userId) => userId !== this.userService.getUser()._id
      );
      this.alertService.warning('Friend deleted!')
    },
    (error) => this.alertService.error(error.message));
  }

  goToProfile(id: string) {
    this.router.navigate(['main/profile', id]);
  }

  goToChat(id: string) {
    this.router.navigate(['main/messages', id]);
  }

  setLimitAndRandomizeFriends(friends: IUser[]): IUser[] {
    let randomFriendsArray: IUser[] = friends.sort(() => Math.random() - 0.5);
    if (friends.length < 6) {
      return friends;
    } else {
      return randomFriendsArray.slice(0, 6);
    }
  }

  isBlocked(): boolean {
    return (
      this.user.isShowProfile ||
      this.user.friends.includes(this.userService.getUser()._id) ||
      this.isCurrentUser
    );
  }
}
