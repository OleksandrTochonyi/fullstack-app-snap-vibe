import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { LoginPageComponent } from './login-page/login-page.component';
import { MainLayoutComponent } from './shared/layouts/main-layout/main-layout.component';
import { AuthLayoutComponent } from './shared/layouts/auth-layout/auth-layout.component';
import { RegisterPageComponent } from './register-page/register-page.component';
import { AuthGuard } from './shared/classes/auth.guard';
import { MessagesPageComponent } from './messages-page/messages-page.component';
import { PeoplePageComponent } from './people-page/people-page.component';
import { NewsPageComponent } from './news-page/news-page.component';
import { ProfilePageComponent } from './profile-page/profile-page.component';
import { EditProfilePageComponent } from './edit-profile-page/edit-profile-page.component';
import { ChatComponent } from './messages-page/components/chat/chat.component';

const routes: Routes = [
  {
    path: '',
    component: AuthLayoutComponent,
    children: [
      { path: '', redirectTo: 'login', pathMatch: 'full' },
      { path: 'login', component: LoginPageComponent },
      { path: 'register', component: RegisterPageComponent },
    ],
  },
  {
    path: 'main',
    component: MainLayoutComponent,
    canActivate: [AuthGuard],
    children: [
      { path: '', redirectTo: 'profile', pathMatch: 'full' },
      {
        path: 'profile/:id',
        component: ProfilePageComponent,
        canActivate: [AuthGuard],
      },
      {
        path: 'profile',
        component: ProfilePageComponent,
        canActivate: [AuthGuard],
        children: [],
      },
      {
        path: 'messages',
        component: MessagesPageComponent,
        canActivate: [AuthGuard],
        children: [
          { path: ':id', component: ChatComponent }
        ],
      },
      {
        path: 'people',
        component: PeoplePageComponent,
        canActivate: [AuthGuard],
      },
      {
        path: 'news',
        component: NewsPageComponent,
        canActivate: [AuthGuard],
      },
      {
        path: 'edit-profile',
        component: EditProfilePageComponent,
        canActivate: [AuthGuard],
      },
    ],
  },

  {
    path: '**',
    redirectTo: '/',
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
