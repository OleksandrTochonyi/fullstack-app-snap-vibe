import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { QuillModule } from 'ngx-quill';

import { AppComponent } from './app.component';
import { LoginPageComponent } from './login-page/login-page.component';
import { AuthLayoutComponent } from './shared/layouts/auth-layout/auth-layout.component';
import { MainLayoutComponent } from './shared/layouts/main-layout/main-layout.component';
import { RegisterPageComponent } from './register-page/register-page.component';
import { ProfilePageComponent } from './profile-page/profile-page.component';
import { PostComponent } from './shared/components/post/post.component';
import { CommentComponent } from './shared/components/comment/comment.component';
import { LoaderComponent } from './shared/components/loader/loader.component';
import { MessagesPageComponent } from './messages-page/messages-page.component';
import { PeoplePageComponent } from './people-page/people-page.component';
import { NewsPageComponent } from './news-page/news-page.component';
import { UserComponent } from './people-page/components/user/user.component';
import { EditProfilePageComponent } from './edit-profile-page/edit-profile-page.component';
import { ChatComponent } from './messages-page/components/chat/chat.component';
import { DialogComponent } from './messages-page/components/dialog/dialog.component';
import { AlertComponent } from './shared/components/alert/alert.component';

import { TokenInterceptor } from './shared/classes/token.interceptor';
import { FriendlistPipe } from './shared/pipes/friend-list.pipe';
import { SearchByNamePipe } from './shared/pipes/search-by-name.pipe';
import { FilterByLocationPipe } from './shared/pipes/filter-by-location.pipe';
import { FilterbyagePipe } from './shared/pipes/filter-by-age.pipe';
import { SplicepostsPipe } from './shared/pipes/splice-posts.pipe';
import { SearchPostByNamePipe } from './shared/pipes/search-post-by-content.pipe';
import { FilterPostsByDatePipe } from './shared/pipes/filter-posts-by-date.pipe';

@NgModule({
  declarations: [
    AppComponent,
    LoginPageComponent,
    AuthLayoutComponent,
    MainLayoutComponent,
    RegisterPageComponent,
    ProfilePageComponent,
    PostComponent,
    CommentComponent,
    LoaderComponent,
    MessagesPageComponent,
    PeoplePageComponent,
    NewsPageComponent,
    UserComponent,
    EditProfilePageComponent,
    FriendlistPipe,
    SearchByNamePipe,
    FilterByLocationPipe,
    FilterbyagePipe,
    SplicepostsPipe,
    SearchPostByNamePipe,
    FilterPostsByDatePipe,
    ChatComponent,
    DialogComponent,
    AlertComponent,
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,    
    QuillModule.forRoot(),
    
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      multi: true,
      useClass: TokenInterceptor,
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
