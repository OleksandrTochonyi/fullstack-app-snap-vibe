import { Component, OnInit, ElementRef, AfterViewInit } from '@angular/core';
import { Parallax } from 'materialize-css';
import { AuthService } from './shared/services/auth.service';
import { UserService } from './shared/services/user.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  sliderInstance: any;

  constructor(
    private elementRef: ElementRef,
    private auth: AuthService,
    private userService: UserService,
  ) {}

  ngOnInit(): void {
    const tokenFromLocalStorage = localStorage.getItem('auth-token');
    const userFromLocalStorage = JSON.parse(localStorage.getItem('user'));
    const expTokenDate = localStorage.getItem('auth-token-expires-in');
    if (tokenFromLocalStorage) {
      this.auth.setToken(tokenFromLocalStorage);
      this.userService.setUser(userFromLocalStorage);
    }
  }

  ngAfterViewInit(): void {
    const parallaxElement =
      this.elementRef.nativeElement.querySelectorAll('.parallax');
    Parallax.init(parallaxElement);
  }
}
