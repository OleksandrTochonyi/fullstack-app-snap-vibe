import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IMessage, IUser } from 'src/app/shared/interfaces';
import { MessagesService } from 'src/app/shared/services/messages.service';
import { UserService } from 'src/app/shared/services/user.service';

@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.scss'],
})
export class DialogComponent implements OnInit {
  user: IUser;
  isCurrentDialog: boolean = false;
  unreadedMessages: IMessage[] = [];

  @Input() userId: string;
  @Input() messages: IMessage[];

  constructor(
    private userService: UserService,
    private route: ActivatedRoute,
    private messageService: MessagesService
  ) {}

  ngOnInit(): void {
    this.unreadedMessages = this.messages.filter(
      (message) => message.status === 'new'
    );
    this.userService.getUserById(this.userId).subscribe((resp) => {
      this.user = resp;
    });
    this.route.params.subscribe((params) => {
      if (params['id'] === this.userId) {
        this.isCurrentDialog = true;
      } else {
        this.isCurrentDialog = false;
      }
    });
  }

  readMessages() {
    if (this.unreadedMessages.length) {
      const readMessagesIds = this.unreadedMessages.map(
        (message) => message._id
      );
      this.messageService
        .markMessageAsRead(readMessagesIds)
        .subscribe((resp) => {
          this.unreadedMessages = [];
          console.log(resp);
        });
    }
  }
}
