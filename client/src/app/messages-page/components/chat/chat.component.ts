import { Component, ElementRef, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import BlotFormatter from 'quill-blot-formatter';
import Quill from 'quill';

import { IMessage, IUser } from 'src/app/shared/interfaces';
import { getQuillsModules } from 'src/app/shared/helpers/quill-editor.model';
import { UserService } from 'src/app/shared/services/user.service';
import { MessagesService } from 'src/app/shared/services/messages.service';
Quill.register('modules/blotFormatter', BlotFormatter);

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss'],
})
export class ChatComponent implements OnInit {
  chatWithUser: IUser;
  messages: IMessage[] = [];
  modules = {};
  isLoading: boolean = true;
  form: FormGroup;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private userService: UserService,
    private messageService: MessagesService,
    private elementRef: ElementRef
  ) {}

  ngOnInit(): void {
    this.modules = getQuillsModules();
    this.form = new FormGroup({
      message: new FormControl('', Validators.required),
    });
    this.route.params.subscribe((params) => {
      if (params['id']) {
        this.userService.getUserById(params['id']).subscribe(
          (resp) => {
            this.chatWithUser = resp;
          },
          (error) => console.log(error)
        );
        this.messageService.getMessagesById(params['id']).subscribe((resp) => {
          this.messages = resp.reverse();
          this.isLoading = false;
        });
      }
    });
  }

  isSender(message: IMessage): boolean {
    if (message.sender !== this.chatWithUser['_id']) {
      return true;
    } else {
      return false;
    }
  }

  goToProfile() {
    this.router.navigate(['/main', 'profile']);
  }

  addMessage() {
    if (this.form.invalid) {
      return;
    }
    const newMessage = {
      content: this.form.value.message,
      recipient: this.chatWithUser['_id'],
    };
    this.messageService.createMessage(newMessage).subscribe((resp) => {
      this.messages.unshift(resp);
      this.form.reset();
    });
  }
}
