import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { MessagesService } from '../shared/services/messages.service';
import { IMessage } from '../shared/interfaces';
import { UserService } from './../shared/services/user.service';

@Component({
  selector: 'app-messages-page',
  templateUrl: './messages-page.component.html',
  styleUrls: ['./messages-page.component.scss'],
})
export class MessagesPageComponent implements OnInit {
  dialogsIdsArr: string[];
  allMessages: IMessage[];
  
  constructor(
    private messageService: MessagesService,
    private userService: UserService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.messageService.getAll().subscribe(
      (resp) => {
        this.getUniqueChats(resp);
      },
      (error) => console.log(error)
    );
  }

  getUniqueChats(messagesArray: IMessage[]) {
    const filteredMessagesArray = messagesArray
      .sort((a: any, b: any) => a.timestamp - b.timestamp)
      .reverse();
    this.allMessages = messagesArray;
    const recepients = [
      ...new Set(filteredMessagesArray.map((message) => message.recipient)),
    ];
    const sender = [
      ...new Set(filteredMessagesArray.map((message) => message.sender)),
    ];
    const newDialogsSet = [
      ...new Set(
        [...recepients, ...sender].filter(
          (el) => el !== this.userService.getUser()._id
        )
      ),
    ];
    this.dialogsIdsArr = newDialogsSet;
  }

  openChat(id: string) {
    this.router.navigate(['main/messages', id]);
  }

  getAllUserMessages(id: string): IMessage[] {
    return this.allMessages.filter((message) => message.sender === id);
  }
}
