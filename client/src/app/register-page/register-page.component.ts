import {
  Component,
  ElementRef,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';

import { ConfirmPasswordValidator } from '../shared/helpers/custom-validators';
import { AuthService } from '../shared/services/auth.service';
import { AlertService } from '../shared/services/alert.service';

@Component({
  selector: 'app-register-page',
  templateUrl: './register-page.component.html',
  styleUrls: ['./register-page.component.scss'],
})
export class RegisterPageComponent implements OnInit, OnDestroy {
  form: FormGroup;
  registerSub: Subscription;
  avatar: File;
  avatarPreview: any = '';
  @ViewChild('input') inputRef: ElementRef;

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private router: Router,
    private alertService: AlertService
  ) {}

  ngOnInit(): void {
    this.form = this.formBuilder.group(
      {
        email: new FormControl('', [Validators.required, Validators.email]),
        password: new FormControl('', [
          Validators.required,
          Validators.minLength(8),
        ]),
        confirmPassword: new FormControl('', [
          Validators.required,
          Validators.minLength(8),
        ]),
        firstName: new FormControl('', Validators.required),
        lastName: new FormControl('', Validators.required),
        country: new FormControl('', Validators.required),
        city: new FormControl('', Validators.required),
        mobileNumber: new FormControl(''),
      },
      {
        validator: ConfirmPasswordValidator('password', 'confirmPassword'),
      }
    );
  }

  ngOnDestroy(): void {
    if (this.registerSub) {
      this.registerSub.unsubscribe();
    }
  }

  triggerClickInput() {
    this.inputRef.nativeElement.click();
  }

  onFileUpload(event: any) {
    const file = event.target.files[0];
    this.avatar = file;

    const reader = new FileReader();

    reader.onload = () => {
      this.avatarPreview = reader.result;
    };

    reader.readAsDataURL(file);
  }

  register() {
    if (this.form.invalid) {
      return;
    }
    const candidate = {
      firstName: this.form.value.firstName,
      lastName: this.form.value.lastName,
      email: this.form.value.email,
      password: this.form.value.password,
      about: {
        country: this.form.value.country,
        city: this.form.value.city,
        mobileNumber: this.form.value.mobileNumber,
      },
    };
    let avatar = null;
    if (this.avatar) {
      avatar = this.avatar;
    }
    this.registerSub = this.authService.register(candidate, avatar).subscribe(
      (resp) => {
        this.router.navigate(['/login']);
        this.alertService.success('You registered successful');
      },
      (error) => this.alertService.error(error.error.message)
    );
  }
}
