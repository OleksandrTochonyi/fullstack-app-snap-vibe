import {
  AfterViewInit,
  Component,
  ElementRef,
  OnInit,
  ViewChild,
} from '@angular/core';

import { PostsService } from '../shared/services/posts.service';
import { IPost } from '../shared/interfaces';
import { MaterializeService } from '../shared/services/materialize.service';

@Component({
  selector: 'app-news-page',
  templateUrl: './news-page.component.html',
  styleUrls: ['./news-page.component.scss'],
})
export class NewsPageComponent implements OnInit, AfterViewInit {
  posts: IPost[];
  postsQuantity = 5;
  isShowAll: boolean = false;
  searchValue: string = '';
  selectedDate: any;

  @ViewChild('datepicker') datepickerRef: ElementRef;

  constructor(
    private postService: PostsService,
    private matService: MaterializeService
  ) {}

  ngOnInit(): void {
    this.postService.getAll(true).subscribe((resp) => {
      this.posts = resp;
    });
  }

  ngAfterViewInit(): void {
    this.initDatepicker();
  }

  initDatepicker() {   
    const options = {
      onClose: () => {
        const selectedDateElement: any = document.querySelector('.datepicker');
        if (selectedDateElement) {
          if (selectedDateElement.value) {
            const selectedDateValue = selectedDateElement.value;
            this.selectedDate = selectedDateValue;
          } else {
            this.selectedDate = '';
          }
        }
      },
    };
    this.matService.initDatepicker(this.datepickerRef, options);
  }

  toggleShowAll() {
    this.isShowAll = !this.isShowAll;
    if (this.isShowAll) {
      this.postsQuantity = 0;
    } else {
      this.postsQuantity = 5;
    }
  }

  clearDate() {
    this.selectedDate = '';
    this.initDatepicker()    
  }
}
